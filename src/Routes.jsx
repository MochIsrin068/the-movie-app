import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import NotFound from "pages/NotFound";
const PageDetail = React.lazy(() => import("pages/PageDetail"));
const PageList = React.lazy(() => import("pages/PageList"));
const PageHome = React.lazy(() => import("pages/PageHome"));
const ProjectRoutes = () => {
  return (
    <React.Suspense fallback={<>Loading...</>}>
      <Router>
        <Routes>
          <Route path="/" element={<PageHome />} />
          <Route path="*" element={<NotFound />} />
          <Route path="/pagelist" element={<PageList />} />
          <Route path="/pagedetail" element={<PageDetail />} />
        </Routes>
      </Router>
    </React.Suspense>
  );
};
export default ProjectRoutes;
