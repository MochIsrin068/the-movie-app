import React from "react";
import PropTypes from "prop-types";

const shapes = { CircleBorder16: "rounded-[16px]" };
const variants = {
  FillGray900cc: "bg-gray_900_cc text-gray_300",
  FillRedA700: "bg-red_A700 text-gray_300",
  FillBlack90033: "bg-black_900_33 text-gray_300",
};
const sizes = { sm: "p-[5px]" };

const Button = ({
  children,
  className = "",
  leftIcon,
  rightIcon,
  shape,
  variant,
  size,
  ...restProps
}) => {
  return (
    <button
      className={`${className} ${(shape && shapes[shape]) || ""} ${
        (size && sizes[size]) || ""
      } ${(variant && variants[variant]) || ""}`}
      {...restProps}
    >
      {!!leftIcon && leftIcon}
      {children}
      {!!rightIcon && rightIcon}
    </button>
  );
};

Button.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node,
  shape: PropTypes.oneOf(["CircleBorder16"]),
  variant: PropTypes.oneOf(["FillGray900cc", "FillRedA700", "FillBlack90033"]),
  size: PropTypes.oneOf(["sm"]),
};

Button.defaultProps = { className: "", shape: "", variant: "", size: "" };
export { Button };
