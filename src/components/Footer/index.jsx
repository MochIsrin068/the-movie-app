import React from "react";

import { Img, Text } from "components";

const Footer = (props) => {
  return (
    <>
      <footer className={props.className}>
        <div className="flex sm:flex-col flex-row sm:gap-10 items-center justify-between mb-[70px] ml-[120px] mr-[130px] mt-[65px] w-[83%]">
          <Text className="font-montserrat text-gray_500" variant="body1">
            © 2021 MoovieTime. All rights reserved.
          </Text>
          <Img
            src="images/img_logo_white_a700.svg"
            className="h-6"
            alt="logo"
          />
          <Text
            className="font-montserrat text-gray_500 text-right"
            variant="body1"
          >
            <>Made with &lt;INSERT_FRAMEWORK&gt;</>
          </Text>
        </div>
      </footer>
    </>
  );
};

Footer.defaultProps = {};

export default Footer;
