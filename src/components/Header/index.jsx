import React from "react";

import { useNavigate } from "react-router-dom";

import { Img, Input, Text } from "components";

import { CloseSVG } from "../../assets/images";

const Header = (props) => {
  const navigate = useNavigate();

  const [searchbarvalue, setSearchbarvalue] = React.useState("");

  return (
    <>
      <header className={props.className}>
        <Img
          src="images/img_moovietimelogo.svg"
          className="common-pointer h-[31px] md:ml-[0] ml-[120px] md:mt-0 my-[17px]"
          alt="moovietimelogo"
          onClick={() => navigate("/")}
        />
        <Input
          value={searchbarvalue}
          onChange={(e) => setSearchbarvalue(e)}
          wrapClassName="flex ml-9 md:ml-[0] md:mt-0 my-[15px] w-2/5 md:w-full"
          className="font-montserrat font-normal leading-[normal] p-0 placeholder:text-gray_300 text-base text-gray_300 text-left w-full"
          name="searchbar"
          placeholder="Find movie"
          prefix={
            <Img
              src="images/img_iconfindericmovie48px3669324_1.svg"
              className="cursor-pointer mr-[13px] my-auto"
              alt="iconfinder_ic_movie_48px_3669324 1"
            />
          }
          suffix={
            <CloseSVG
              className="cursor-pointer my-auto"
              onClick={() => setSearchbarvalue("")}
              fillColor="#e5e5e5"
              style={{
                visibility: searchbarvalue?.length <= 0 ? "hidden" : "visible",
              }}
              height={24}
              width={24}
              viewBox="0 0 24 24"
            />
          }
          shape="srcRoundedBorder4"
          size="smSrc"
          variant="srcFillBlack90021"
        ></Input>
        <div className="flex flex-row gap-[11px] items-center justify-center md:ml-[0] ml-[45px] md:mt-0 my-[23px] w-[9%] md:w-full">
          <Img src="images/img_grid.svg" className="h-5 w-5" alt="grid" />
          <Text
            className="font-montserrat font-semibold text-gray_300 uppercase"
            as="h6"
            variant="h6"
          >
            Categories
          </Text>
        </div>
        <ul className="flex sm:flex-col flex-row sm:hidden items-center justify-center ml-12 md:ml-[0] mr-[120px] md:mt-0 my-[23px] w-[19%] md:w-full common-row-list">
          <li>
            <Text
              className="font-montserrat font-semibold text-gray_300 uppercase"
              as="h6"
              variant="h6"
            >
              Movies
            </Text>
          </li>
          <li>
            <Text
              className="font-montserrat font-semibold ml-[39px] text-gray_300 uppercase"
              as="h6"
              variant="h6"
            >
              TV Shows
            </Text>
          </li>
          <li>
            <Text
              className="font-montserrat font-semibold ml-[38px] text-gray_300 uppercase"
              as="h6"
              variant="h6"
            >
              Login
            </Text>
          </li>
        </ul>
      </header>
    </>
  );
};

Header.defaultProps = {};

export default Header;
