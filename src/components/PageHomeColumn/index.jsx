import React from "react";

import { Img } from "components";

const PageHomeColumn = (props) => {
  return (
    <>
      <div className={props.className}>
        <div className="flex flex-col h-4 items-center justify-start w-4">
          <Img
            src="images/img_star1.svg"
            className="h-4 rounded-bl-[1px] rounded-br-[1px] w-4"
            alt="starOne"
          />
        </div>
      </div>
    </>
  );
};

PageHomeColumn.defaultProps = {};

export default PageHomeColumn;
