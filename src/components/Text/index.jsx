import React from "react";

const variantClasses = {
  h1: "font-semibold text-4xl sm:text-[32px] md:text-[34px]",
  h2: "font-medium sm:text-2xl md:text-[26px] text-[28px]",
  h3: "font-semibold text-2xl md:text-[22px] sm:text-xl",
  h4: "text-lg",
  h5: "text-base",
  h6: "text-sm",
  body1: "font-normal text-[13px]",
  body2: "text-xs",
};

const Text = ({ children, className = "", variant, as, ...restProps }) => {
  const Component = as || "span";
  return (
    <Component
      className={`${className} ${variant && variantClasses[variant]}`}
      {...restProps}
    >
      {children}
    </Component>
  );
};

export { Text };
