import React from "react";

import { Button, Img, Line, List, Text } from "components";
import Footer from "components/Footer";
import Header from "components/Header";

const PageDetail = () => {
  return (
    <>
      <div className="bg-gray_900_01 flex flex-col font-montserrat gap-[50px] items-center justify-start mx-auto w-full">
        <div className="h-[1009px] sm:h-[810px] md:h-[936px] md:px-5 relative w-full">
          <div className="h-[1009px] sm:h-[810px] md:h-[936px] m-auto w-full">
            <Img
              src="images/img_image1_810x1440.png"
              className="absolute h-[810px] inset-x-[0] mx-auto object-cover top-[0] w-full"
              alt="imageOne"
            />
            <div
              className="absolute bg-cover bg-no-repeat bottom-[0] flex flex-col h-[621px] inset-x-[0] items-center justify-end mx-auto p-[58px] md:px-10 sm:px-5 w-full"
              style={{ backgroundImage: "url('images/img_group17.svg')" }}
            >
              <div className="flex flex-col items-center justify-start mt-[179px] w-[91%] md:w-full">
                <div className="flex flex-col gap-[23px] justify-start w-full">
                  <Text
                    className="font-semibold md:ml-[0] ml-[7px] text-red_A700"
                    as="h6"
                    variant="h6"
                  >
                    REVIEWS
                  </Text>
                  <List
                    className="sm:flex-col flex-row gap-[34px] grid md:grid-cols-1 grid-cols-2 justify-center w-full"
                    orientation="horizontal"
                  >
                    <div className="bg-gray_50 flex flex-1 flex-col items-center justify-start p-5 rounded-[14px] shadow-bs w-full">
                      <div className="flex flex-col gap-[25px] items-center justify-start w-[99%] md:w-full">
                        <div className="flex sm:flex-col flex-row sm:gap-5 items-start justify-start w-full">
                          <div className="bg-gray_900_35 h-12 sm:mt-0 my-0.5 rounded-[50%] w-12"></div>
                          <div className="flex flex-col items-start justify-start sm:ml-[0] ml-[18px] sm:mt-0 mt-[11px]">
                            <Text
                              className="font-bold text-gray_900_03"
                              as="h6"
                              variant="h6"
                            >
                              SWITCH.
                            </Text>
                            <Text
                              className="font-normal mt-0.5 text-gray_700"
                              variant="body2"
                            >
                              December 18, 2020
                            </Text>
                          </div>
                          <div className="bg-gray_400_47 flex sm:flex-1 flex-row gap-1.5 items-start justify-center ml-64 sm:ml-[0] p-0.5 rounded-[7px] w-[19%] sm:w-full">
                            <Img
                              src="images/img_star1_17x17.svg"
                              className="h-[17px] mb-[21px] ml-[3px] mt-2 rounded-[1px] w-[17px]"
                              alt="starOne"
                            />
                            <Text
                              className="mr-3 mt-0.5 text-black_900"
                              as="h1"
                              variant="h1"
                            >
                              7.0
                            </Text>
                          </div>
                        </div>
                        <Text
                          className="italic leading-[150.00%] text-black_900 w-full"
                          variant="body1"
                        >
                          <span className="text-black_900 text-[13px] font-montserrat text-left font-normal">
                            <>
                              It isn&#39;t as easy as saying &#39;Wonder Woman
                              1984&#39; is a good or bad movie. The pieces are
                              there, and there are moments I adore, but it does
                              come across as a bit of a mess, even though the
                              action sequences are breathtaking. If you&#39;re a
                              fan of the original film, you&#39;ll be more
                              willing to take the ride, but for those more
                              indifferent, it may be a bit of a blander sit. If
                              you can and are planning to watch it, the
                              theatrical experience is the way to go - there is
                              nothing like seeing these stunning sets, fun
                              action scenes and hearing Zimmer&#39;s
                              jaw-dropping score like on the big screen. - Chris
                              dos Santos...{" "}
                            </>
                          </span>
                          <a
                            href="javascript:"
                            className="text-red_500 text-[13px] font-montserrat text-left font-normal underline"
                          >
                            read the rest.
                          </a>
                        </Text>
                      </div>
                    </div>
                    <div className="bg-gray_50 flex flex-1 flex-col items-center justify-start p-5 rounded-[14px] shadow-bs w-full">
                      <div className="flex flex-col gap-[25px] items-center justify-start w-[99%] md:w-full">
                        <div className="flex sm:flex-col flex-row sm:gap-5 items-start justify-start w-full">
                          <div className="bg-gray_900_35 h-12 sm:mt-0 my-0.5 rounded-[50%] w-12"></div>
                          <div className="flex flex-col items-start justify-start sm:ml-[0] ml-[18px] sm:mt-0 mt-2.5">
                            <Text
                              className="font-bold text-gray_900_03"
                              as="h6"
                              variant="h6"
                            >
                              msbreviews
                            </Text>
                            <Text
                              className="font-normal mt-[3px] text-gray_700"
                              variant="body2"
                            >
                              December 18, 2020
                            </Text>
                          </div>
                          <div className="bg-gray_400_47 flex sm:flex-1 flex-row gap-1.5 items-start justify-center ml-64 sm:ml-[0] p-0.5 rounded-[7px] w-[19%] sm:w-full">
                            <Img
                              src="images/img_star1_2.svg"
                              className="h-[17px] mb-[21px] ml-[3px] mt-2 rounded-[1px] w-[17px]"
                              alt="starOne"
                            />
                            <Text
                              className="mr-2 mt-0.5 text-black_900"
                              as="h1"
                              variant="h1"
                            >
                              8.0
                            </Text>
                          </div>
                        </div>
                        <Text
                          className="italic leading-[150.00%] text-black_900"
                          variant="body1"
                        >
                          <>
                            If you enjoy reading my Spoiler-Free reviews, please
                            follow my blog @ https://www.msbreviews.com
                            <br />
                            The superhero genre has been growing exponentially
                            during the last decade, so it&#39;s bizarre to go
                            through an entire year with only Birds of Prey and
                            The New Mutants instead of literally dozens of films
                            from both Marvel and DC. Thankfully, Warner Bros.
                            decided to release Wonder Woman 1984 before the
                            year&#39;s end, but not without a catch. Most people
                            will only have the possibility of watching one of
                            the few blockbusters of 2020 through HBO Max, a
                            streaming service only{" "}
                          </>
                        </Text>
                      </div>
                    </div>
                  </List>
                </div>
              </div>
            </div>
          </div>
          <div className="absolute flex flex-col items-center justify-start left-[9%] top-1/4 w-[74%]">
            <div className="flex md:flex-col flex-row gap-[29px] items-end justify-between w-full">
              <Img
                src="images/img_image1_330x220.png"
                className="h-[330px] md:h-auto object-cover"
                alt="imageOne_One"
              />
              <div className="flex flex-col gap-[51px] justify-start md:mt-0 mt-4">
                <div className="flex flex-col items-start justify-start w-full">
                  <Text
                    className="font-medium ml-1 md:ml-[0] text-white_A700"
                    as="h4"
                    variant="h4"
                  >
                    2020
                  </Text>
                  <Text
                    className="ml-1 md:ml-[0] text-gray_300"
                    as="h1"
                    variant="h1"
                  >
                    Wonder Woman 1984
                  </Text>
                  <Text
                    className="font-medium ml-1 md:ml-[0] mt-[5px] text-white_A700"
                    as="h6"
                    variant="h6"
                  >
                    Fantasy, Action, Adventure
                  </Text>
                  <div className="flex flex-col items-center justify-start mt-[46px] w-full">
                    <div className="flex sm:flex-col flex-row sm:gap-10 items-start justify-between w-full">
                      <Img
                        src="images/img_star2.svg"
                        className="h-8 sm:mt-0 my-[5px] rounded-[1px] w-8"
                        alt="starTwo"
                      />
                      <Text className="text-gray_300" as="h1" variant="h1">
                        7.0
                      </Text>
                      <div className="flex flex-col items-start justify-start sm:mt-0 mt-1">
                        <Text
                          className="font-medium text-white_A700_7f uppercase"
                          variant="body2"
                        >
                          User Score
                        </Text>
                        <Text
                          className="font-medium mt-[3px] text-white_A700 uppercase"
                          variant="body2"
                        >
                          3621 Votes
                        </Text>
                      </div>
                      <Line className="bg-white_A700_33 h-[41px] sm:h-px sm:mt-0 mt-0.5 sm:w-full w-px" />
                      <div className="flex flex-col items-start justify-start sm:mt-0 mt-1">
                        <Text
                          className="font-medium text-white_A700_7f uppercase"
                          variant="body2"
                        >
                          Status
                        </Text>
                        <Text
                          className="font-medium mt-[3px] text-white_A700 uppercase"
                          variant="body2"
                        >
                          Released
                        </Text>
                      </div>
                      <Line className="bg-white_A700_33 h-[41px] sm:h-px sm:mt-0 mt-0.5 sm:w-full w-px" />
                      <div className="flex flex-col items-start justify-start sm:mt-0 mt-1">
                        <Text
                          className="font-medium text-white_A700_7f uppercase"
                          variant="body2"
                        >
                          language
                        </Text>
                        <Text
                          className="font-medium mt-[3px] text-white_A700 uppercase"
                          variant="body2"
                        >
                          english
                        </Text>
                      </div>
                      <Line className="bg-white_A700_33 h-[41px] sm:h-px sm:mt-0 mt-0.5 sm:w-full w-px" />
                      <div className="flex flex-col items-start justify-start sm:mt-0 mt-1">
                        <Text
                          className="font-medium text-white_A700_7f uppercase"
                          variant="body2"
                        >
                          budget
                        </Text>
                        <Text
                          className="font-medium mt-[3px] text-white_A700 uppercase"
                          variant="body2"
                        >
                          $200,000,000.00
                        </Text>
                      </div>
                      <Line className="bg-white_A700_33 h-[41px] sm:h-px sm:mt-0 mt-0.5 sm:w-full w-px" />
                      <div className="flex flex-col items-start justify-start sm:mt-0 mt-1">
                        <Text
                          className="font-medium text-white_A700_7f uppercase"
                          variant="body2"
                        >
                          production
                        </Text>
                        <Text
                          className="font-medium mt-[3px] text-white_A700 uppercase"
                          variant="body2"
                        >
                          DC Entertainment
                        </Text>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="flex flex-col gap-[7px] items-start justify-start md:ml-[0] ml-[9px]">
                  <Text
                    className="font-semibold text-red_A700"
                    as="h6"
                    variant="h6"
                  >
                    OVERVIEW
                  </Text>
                  <Text
                    className="font-normal leading-[200.00%] text-black_900 w-full"
                    as="h6"
                    variant="h6"
                  >
                    Wonder Woman comes into conflict with the Soviet Union
                    during the Cold War in the 1980s and finds a formidable foe
                    by the name of the Cheetah.
                  </Text>
                </div>
              </div>
            </div>
          </div>
          <Header className="absolute bg-white_A700_0c flex flex-row inset-x-[0] items-center justify-center mx-auto top-[0] w-full" />
        </div>
        <div className="flex flex-col items-center justify-start max-w-[1186px] mx-auto md:px-5 w-full">
          <div className="flex flex-col gap-[35px] items-start justify-start w-full">
            <Text
              className="font-semibold text-white_A700"
              as="h6"
              variant="h6"
            >
              RECOMMENDATION MOVIES
            </Text>
            <List
              className="sm:flex-col flex-row gap-[21px] grid sm:grid-cols-1 md:grid-cols-3 grid-cols-5 justify-center w-full"
              orientation="horizontal"
            >
              <div className="flex flex-1 flex-col items-start justify-start w-full">
                <div className="h-[330px] relative w-full">
                  <Img
                    src="images/img_image1_3.png"
                    className="h-[330px] m-auto object-cover w-full"
                    alt="imageOne"
                  />
                  <Button
                    className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                    size="sm"
                    variant="FillGray900cc"
                  >
                    7.0
                  </Button>
                </div>
                <Text
                  className="font-semibold mt-[13px] text-gray_300"
                  as="h5"
                  variant="h5"
                >
                  The New Mutants
                </Text>
                <Text
                  className="font-normal mt-0.5 text-gray_500"
                  as="h6"
                  variant="h6"
                >
                  2020
                </Text>
              </div>
              <div className="flex flex-1 flex-col items-start justify-start w-full">
                <div className="h-[330px] relative w-full">
                  <Img
                    src="images/img_image1_4.png"
                    className="h-[330px] m-auto object-cover w-full"
                    alt="imageOne"
                  />
                  <Button
                    className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                    size="sm"
                    variant="FillGray900cc"
                  >
                    7.0
                  </Button>
                </div>
                <Text
                  className="font-semibold mt-[13px] text-gray_300"
                  as="h5"
                  variant="h5"
                >
                  Soul
                </Text>
                <Text
                  className="font-normal mt-0.5 text-gray_500"
                  as="h6"
                  variant="h6"
                >
                  2020
                </Text>
              </div>
              <div className="flex flex-1 flex-col items-start justify-start w-full">
                <div className="h-[330px] relative w-full">
                  <Img
                    src="images/img_image1_5.png"
                    className="h-[330px] m-auto object-cover w-full"
                    alt="imageOne"
                  />
                  <Button
                    className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                    size="sm"
                    variant="FillGray900cc"
                  >
                    7.0
                  </Button>
                </div>
                <Text
                  className="font-semibold mt-[13px] text-gray_300"
                  as="h5"
                  variant="h5"
                >
                  Tenet
                </Text>
                <Text
                  className="font-normal mt-0.5 text-gray_500"
                  as="h6"
                  variant="h6"
                >
                  2020
                </Text>
              </div>
              <div className="flex flex-1 flex-col items-start justify-start w-full">
                <div className="h-[330px] relative w-full">
                  <Img
                    src="images/img_image1_6.png"
                    className="h-[330px] m-auto object-cover w-full"
                    alt="imageOne"
                  />
                  <Button
                    className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                    size="sm"
                    variant="FillGray900cc"
                  >
                    7.0
                  </Button>
                </div>
                <Text
                  className="font-semibold mt-[13px] text-gray_300"
                  as="h5"
                  variant="h5"
                >
                  The Old Guard
                </Text>
                <Text
                  className="font-normal mt-0.5 text-gray_500"
                  as="h6"
                  variant="h6"
                >
                  2020
                </Text>
              </div>
              <div className="flex flex-1 flex-col items-start justify-start w-full">
                <div className="h-[330px] relative w-full">
                  <Img
                    src="images/img_image1_7.png"
                    className="h-[330px] m-auto object-cover w-full"
                    alt="imageOne"
                  />
                  <Button
                    className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                    size="sm"
                    variant="FillGray900cc"
                  >
                    7.0
                  </Button>
                </div>
                <Text
                  className="font-semibold mt-3.5 text-gray_300"
                  as="h5"
                  variant="h5"
                >
                  Project Power
                </Text>
                <Text
                  className="font-normal text-gray_500"
                  as="h6"
                  variant="h6"
                >
                  2020
                </Text>
              </div>
            </List>
          </div>
        </div>
        <Footer className="bg-black_900_49 flex items-center justify-center md:px-5 w-full" />
      </div>
    </>
  );
};

export default PageDetail;
