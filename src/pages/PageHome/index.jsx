import React from "react";

import {
  Button,
  Img,
  Line,
  List,
  PagerIndicator,
  Slider,
  Text,
} from "components";
import Header from "components/Header";
import PageHomeColumn from "components/PageHomeColumn";

const PageHome = () => {
  const sliderRef = React.useRef(null);
  const [sliderState, setsliderState] = React.useState(0);

  return (
    <>
      <div className="bg-gray_900 flex flex-col font-montserrat items-center justify-start mx-auto overflow-auto w-full">
        <Header className="bg-white_A700_0c flex flex-row items-center justify-center md:px-5 w-full" />
        <div className="flex flex-col gap-[52px] items-center justify-start max-w-[1691px] mt-14 mx-auto md:px-5 w-full">
          <Slider
            autoPlay
            autoPlayInterval={2000}
            paddingLeft={1546}
            activeIndex={sliderState}
            responsive={{
              0: { items: 1 },
              550: { items: 1 },
              1050: { items: 1 },
            }}
            onSlideChanged={(e) => {
              setsliderState(e?.item);
            }}
            ref={sliderRef}
            className="w-full"
            items={[...Array(4)].map(() => (
              <React.Fragment key={Math.random()}>
                <List
                  className="sm:flex-col flex-row gap-[34px] grid sm:grid-cols-1 md:grid-cols-2 grid-cols-3 justify-center mx-2.5"
                  orientation="horizontal"
                >
                  <div className="h-[364px] sm:ml-[0] relative w-full">
                    <div className="absolute bg-black_900 flex flex-col h-max inset-y-[0] items-end justify-start my-auto p-[25px] sm:px-5 right-[0] w-[96%]">
                      <div className="flex flex-col items-start justify-start mb-[27px] mr-1.5 w-[52%] md:w-full">
                        <div className="flex flex-row gap-[5px] items-start justify-start w-1/5 md:w-full">
                          <PageHomeColumn className="flex flex-col h-4 items-center justify-start mt-0.5 w-4 sm:w-full" />
                          <Text
                            className="font-bold text-white_A700"
                            as="h4"
                            variant="h4"
                          >
                            7.2
                          </Text>
                        </div>
                        <Text
                          className="mt-[3px] text-white_A700"
                          as="h2"
                          variant="h2"
                        >
                          <>
                            News of the
                            <br />
                            World
                          </>
                        </Text>
                        <div className="flex flex-row items-center justify-start mt-[19px] w-[45%] md:w-full">
                          <Text
                            className="font-normal text-white_A700"
                            as="h5"
                            variant="h5"
                          >
                            2021
                          </Text>
                          <div className="bg-white_A700_7f h-1.5 ml-1.5 my-1.5 rounded-[50%] w-1.5"></div>
                          <Text
                            className="font-normal ml-[5px] text-white_A700"
                            as="h5"
                            variant="h5"
                          >
                            Drama
                          </Text>
                        </div>
                        <Text
                          className="font-normal leading-[18.00px] mt-[11px] text-white_A700 w-full"
                          variant="body2"
                        >
                          A Texan traveling across the wild West bringing the
                          news of the world to local townspeople, agrees to help
                          rescue a young girl who was kidnapped.
                        </Text>
                      </div>
                    </div>
                    <Img
                      src="images/img_image12.png"
                      className="absolute h-[364px] inset-y-[0] left-[0] my-auto object-cover w-[45%]"
                      alt="imageTwelve"
                    />
                  </div>
                  <div className="h-[364px] sm:ml-[0] relative w-full">
                    <div className="absolute bg-black_900 flex flex-col h-max inset-y-[0] items-end justify-start my-auto p-5 right-[0] w-[96%]">
                      <div className="flex flex-col items-start justify-start mb-[46px] mt-[5px] w-[53%] md:w-full">
                        <div className="flex flex-row gap-[5px] items-center justify-start w-[19%] md:w-full">
                          <PageHomeColumn className="flex flex-col h-4 items-center justify-start w-4 sm:w-full" />
                          <Text
                            className="font-bold text-white_A700"
                            as="h4"
                            variant="h4"
                          >
                            7.3
                          </Text>
                        </div>
                        <Text
                          className="mt-[5px] text-white_A700"
                          as="h2"
                          variant="h2"
                        >
                          Space Sweepers
                        </Text>
                        <div className="flex flex-row items-center justify-start mt-[7px] w-[38%] md:w-full">
                          <Text
                            className="font-normal text-white_A700"
                            as="h5"
                            variant="h5"
                          >
                            2021
                          </Text>
                          <div className="bg-white_A700_7f h-1.5 ml-1.5 my-1.5 rounded-[50%] w-1.5"></div>
                          <Text
                            className="font-normal ml-[5px] text-white_A700"
                            as="h5"
                            variant="h5"
                          >
                            Sci-Fi
                          </Text>
                        </div>
                        <Text
                          className="font-normal leading-[18.00px] mt-[11px] text-white_A700 w-full"
                          variant="body2"
                        >
                          <>
                            When the crew of a space junk collector ship called
                            The Victory discovers a humanoid robot named Dorothy
                            that&#39;s known to be a weapon of mass destruction,
                            they get involved in a risky business deal which
                            puts their lives at stake.
                          </>
                        </Text>
                      </div>
                    </div>
                    <Img
                      src="images/img_image12_364x243.png"
                      className="absolute h-[364px] inset-y-[0] left-[0] my-auto object-cover w-[45%]"
                      alt="imageTwelve"
                    />
                  </div>
                  <div className="h-[364px] sm:ml-[0] relative w-full">
                    <div className="absolute bg-black_900 flex flex-col h-max inset-y-[0] items-end justify-start my-auto p-[25px] sm:px-5 right-[0] w-[96%]">
                      <div className="flex flex-col items-start justify-start mb-2.5 mr-1.5 w-[52%] md:w-full">
                        <div className="flex flex-row gap-[5px] items-start justify-start w-[19%] md:w-full">
                          <PageHomeColumn className="flex flex-col h-4 items-center justify-start mt-0.5 w-4 sm:w-full" />
                          <Text
                            className="font-bold text-white_A700"
                            as="h4"
                            variant="h4"
                          >
                            8.1
                          </Text>
                        </div>
                        <Text
                          className="mt-[3px] text-white_A700"
                          as="h2"
                          variant="h2"
                        >
                          <>
                            To All the Boys: Always and
                            <br />
                            Forever
                          </>
                        </Text>
                        <div className="flex flex-row items-center justify-start mt-[5px] w-[45%] md:w-full">
                          <Text
                            className="font-normal text-white_A700"
                            as="h5"
                            variant="h5"
                          >
                            2021
                          </Text>
                          <div className="bg-white_A700_7f h-1.5 ml-[7px] my-1.5 rounded-[50%] w-1.5"></div>
                          <Text
                            className="font-normal ml-[5px] text-white_A700"
                            as="h5"
                            variant="h5"
                          >
                            Drama
                          </Text>
                        </div>
                        <Text
                          className="font-normal leading-[18.00px] mt-[17px] text-white_A700 w-full"
                          variant="body2"
                        >
                          Senior year of high school takes center stage as Lara
                          Jean returns from a family trip to Korea and considers
                          her college plans — with and without Peter.
                        </Text>
                      </div>
                    </div>
                    <Img
                      src="images/img_image12_1.png"
                      className="absolute h-[364px] inset-y-[0] left-[0] my-auto object-cover w-[45%]"
                      alt="imageTwelve"
                    />
                  </div>
                </List>
              </React.Fragment>
            ))}
            renderDotsItem={({ isActive }) => {
              if (isActive) {
                return (
                  <div className="inline-block cursor-pointer h-3 bg-red_500 w-[60px] rounded-md" />
                );
              }
              return (
                <div
                  className="inline-block cursor-pointer rounded-[50%] h-3 bg-white_A700_7f w-3"
                  role="button"
                  tabIndex={0}
                />
              );
            }}
          />
          <PagerIndicator
            className="flex h-3 justify-center w-[148px]"
            count={4}
            activeCss="inline-block cursor-pointer h-3 bg-red_500 w-[60px] rounded-md"
            activeIndex={sliderState}
            inactiveCss="inline-block cursor-pointer rounded-[50%] h-3 bg-white_A700_7f w-3"
            sliderRef={sliderRef}
            selectedWrapperCss="inline-block md:ml-[0] mx-[8.00px] sm:ml-[0]"
            unselectedWrapperCss="inline-block md:ml-[0] mx-[8.00px] sm:ml-[0]"
          />
        </div>
        <div className="flex flex-col md:gap-10 gap-[114px] items-center justify-start mt-[47px] w-full">
          <div className="flex flex-col md:px-5 relative w-full">
            <div className="bg-white_A700_0c flex sm:flex-col flex-row md:gap-10 items-end justify-between mx-auto p-[89px] md:px-10 sm:px-5 w-full">
              <div className="flex sm:flex-1 flex-col gap-2.5 items-start justify-start mb-[108px] sm:ml-[0] ml-[30px] w-[16%] sm:w-full">
                <Line className="bg-red_500 h-1.5 w-[57%]" />
                <Text className="text-gray_300" as="h3" variant="h3">
                  Discover Movies
                </Text>
              </div>
              <div className="flex sm:flex-1 flex-row gap-[21px] items-center justify-center mb-[111px] mr-[41px] sm:mt-0 mt-3 w-[21%] sm:w-full">
                <Button
                  className="cursor-pointer font-semibold leading-[normal] min-w-[107px] text-center text-gray_300 text-sm"
                  shape="CircleBorder16"
                  size="sm"
                  variant="FillRedA700"
                >
                  Popularity
                </Button>
                <Button
                  className="cursor-pointer font-semibold leading-[normal] min-w-[128px] text-center text-gray_300 text-sm"
                  shape="CircleBorder16"
                  size="sm"
                  variant="FillBlack90033"
                >
                  Release Date
                </Button>
              </div>
            </div>
            <div className="flex flex-col items-center justify-start mt-[-154px] mx-auto w-[84%] z-[1]">
              <div className="flex flex-col items-center justify-start w-full">
                <div className="md:gap-5 gap-[25px] grid sm:grid-cols-1 md:grid-cols-3 grid-cols-5 justify-center min-h-[auto] w-full">
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image1.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageOne"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        7.0
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-[13px] text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      Wonder Woman 1984
                    </Text>
                    <Text
                      className="font-normal mt-0.5 text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2020
                    </Text>
                  </div>
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image11.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageEleven"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        6.4
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-[13px] text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      Below Zero
                    </Text>
                    <Text
                      className="font-normal mt-0.5 text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2021
                    </Text>
                  </div>
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image3.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageThree"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        6.3
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-3.5 text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      The Little Things
                    </Text>
                    <Text
                      className="font-normal text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2021
                    </Text>
                  </div>
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image4.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageFour"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        6.5
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-3 text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      Outside the Wire
                    </Text>
                    <Text
                      className="font-normal mt-[3px] text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2021
                    </Text>
                  </div>
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image5.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageFive"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        5.1
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-3.5 text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      Black Water: Abyss
                    </Text>
                    <Text
                      className="font-normal text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2020
                    </Text>
                  </div>
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image6.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageSix"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        4.6
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-[13px] text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      Breach
                    </Text>
                    <Text
                      className="font-normal mt-0.5 text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2021
                    </Text>
                  </div>
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image7.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageSeven"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        8.3
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-[13px] text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      Soul
                    </Text>
                    <Text
                      className="font-normal mt-0.5 text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2020
                    </Text>
                  </div>
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image8.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageEight"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        6.9
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-3 text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      Fast & Furious Present..
                    </Text>
                    <Text
                      className="font-normal mt-[3px] text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2019
                    </Text>
                  </div>
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image9.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageNine"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        7.6
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-3.5 text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      The Croods: A New Age
                    </Text>
                    <Text
                      className="font-normal text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2020
                    </Text>
                  </div>
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image10.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageTen"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        6.3
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-3.5 text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      Vanguard
                    </Text>
                    <Text
                      className="font-normal text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2020
                    </Text>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <footer className="bg-black_900_49 flex items-center justify-center md:px-5 w-full">
            <div className="flex sm:flex-col flex-row sm:gap-10 items-center justify-between mb-[70px] mt-[65px] mx-auto w-[84%]">
              <Text className="text-gray_500" variant="body1">
                © 2021 MoovieTime. All rights reserved.
              </Text>
              <Img
                src="images/img_moovietimelogogrey.svg"
                className="h-6"
                alt="moovietimelogog"
              />
              <Text className="text-gray_500 text-right" variant="body1">
                <>Made with &lt;INSERT_FRAMEWORK&gt;</>
              </Text>
            </div>
          </footer>
        </div>
      </div>
    </>
  );
};

export default PageHome;
