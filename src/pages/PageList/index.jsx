import React from "react";

import { Button, CheckBox, Img, Line, Text } from "components";
import Footer from "components/Footer";
import Header from "components/Header";

const PageList = () => {
  return (
    <>
      <div className="bg-gray_900_01 flex flex-col font-montserrat items-start justify-start mx-auto w-full">
        <div className="flex flex-col items-center w-full">
          <Header className="bg-white_A700_0c flex flex-row items-center justify-center md:px-5 w-full" />
          <div className="flex flex-col md:px-5 relative w-full">
            <div className="bg-white_A700_0c flex flex-col items-start justify-start mx-auto p-[59px] md:px-10 sm:px-5 w-full">
              <div className="flex flex-col gap-[11px] items-start justify-start mb-[143px] md:ml-[0] ml-[66px] w-[10%] md:w-full">
                <Line className="bg-red_500 h-1.5 w-[87%]" />
                <Text className="text-gray_300" as="h1" variant="h1">
                  Movies
                </Text>
              </div>
            </div>
            <div className="flex flex-col items-center justify-start ml-auto mr-[105px] mt-[-139px] w-[66%] z-[1]">
              <div className="flex flex-col items-center justify-start w-full">
                <div className="gap-5 grid sm:grid-cols-1 md:grid-cols-2 grid-cols-4 justify-center min-h-[auto] w-full">
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image1.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageOne"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        7.0
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-[13px] text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      Wonder Woman 1984
                    </Text>
                    <Text
                      className="font-normal mt-0.5 text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2020
                    </Text>
                  </div>
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image11.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageEleven"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        6.4
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-[13px] text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      Below Zero
                    </Text>
                    <Text
                      className="font-normal mt-0.5 text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2021
                    </Text>
                  </div>
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image3.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageThree"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        6.3
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-3.5 text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      The Little Things
                    </Text>
                    <Text
                      className="font-normal text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2021
                    </Text>
                  </div>
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image4.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageFour"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        6.5
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-3 text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      Outside the Wire
                    </Text>
                    <Text
                      className="font-normal mt-[3px] text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2021
                    </Text>
                  </div>
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image5.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageFive"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        5.1
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-3.5 text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      Black Water: Abyss
                    </Text>
                    <Text
                      className="font-normal text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2020
                    </Text>
                  </div>
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image6.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageSix"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        4.6
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-[13px] text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      Breach
                    </Text>
                    <Text
                      className="font-normal mt-0.5 text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2021
                    </Text>
                  </div>
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image7.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageSeven"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        8.3
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-[13px] text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      Soul
                    </Text>
                    <Text
                      className="font-normal mt-0.5 text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2020
                    </Text>
                  </div>
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image8.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageEight"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        6.9
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-3 text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      Fast & Furious Present..
                    </Text>
                    <Text
                      className="font-normal mt-[3px] text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2019
                    </Text>
                  </div>
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image9.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageNine"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        7.6
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-3.5 text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      The Croods: A New Age
                    </Text>
                    <Text
                      className="font-normal text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2020
                    </Text>
                  </div>
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image10.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageTen"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        6.3
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-3.5 text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      Vanguard
                    </Text>
                    <Text
                      className="font-normal text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2020
                    </Text>
                  </div>
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image9_330x220.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageNine"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        7.3
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-[13px] text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      Tenet
                    </Text>
                    <Text
                      className="font-normal mt-0.5 text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2020
                    </Text>
                  </div>
                  <div className="flex flex-1 flex-col items-start justify-start w-full">
                    <div className="h-[330px] relative w-full">
                      <Img
                        src="images/img_image10_330x220.png"
                        className="h-[330px] m-auto object-cover w-full"
                        alt="imageTen"
                      />
                      <Button
                        className="absolute cursor-pointer font-bold leading-[normal] min-w-[48px] right-[0] text-center text-gray_300 text-lg top-[0]"
                        size="sm"
                        variant="FillGray900cc"
                      >
                        5.9
                      </Button>
                    </div>
                    <Text
                      className="font-semibold mt-[13px] text-gray_300"
                      as="h5"
                      variant="h5"
                    >
                      100% Wolf
                    </Text>
                    <Text
                      className="font-normal mt-[3px] text-gray_500"
                      as="h6"
                      variant="h6"
                    >
                      2020
                    </Text>
                  </div>
                </div>
              </div>
            </div>
            <div className="bg-gradient  flex flex-col gap-4 justify-start ml-[125px] mt-[-143px] py-[21px] rounded-lg w-[17%] z-[1]">
              <Text
                className="font-semibold md:ml-[0] ml-[18px] text-gray_300"
                as="h5"
                variant="h5"
              >
                Sort Result By
              </Text>
              <div className="flex flex-col items-start justify-start mb-8 w-full">
                <Line className="bg-white_A700_11 h-px w-full" />
                <div className="bg-gray_300_21 flex flex-row items-start justify-between md:ml-[0] ml-[19px] mt-[17px] p-[7px] rounded w-[85%] md:w-full">
                  <Text
                    className="font-normal ml-[9px] mt-0.5 text-gray_300"
                    as="h6"
                    variant="h6"
                  >
                    Popularity
                  </Text>
                  <Img
                    src="images/img_arrowdown.svg"
                    className="h-2.5 mt-1 w-2.5"
                    alt="arrowdown"
                  />
                </div>
                <Line className="bg-white_A700_11 h-px mt-[31px] w-full" />
                <Text
                  className="font-semibold md:ml-[0] ml-[18px] mt-2.5 text-gray_300"
                  as="h5"
                  variant="h5"
                >
                  Genres
                </Text>
                <Line className="bg-white_A700_11 h-px mt-3 w-full" />
                <div className="h-[327px] md:h-[344px] md:ml-[0] ml-[19px] mt-[17px] relative w-[85%]">
                  <CheckBox
                    className="font-normal leading-[normal] m-auto sm:pr-5 text-left text-sm text-white_A700"
                    inputClassName="absolute h-3.5 mr-[5px] w-3.5"
                    name="actionadventure_One"
                    id="actionadventure_One"
                    label="&lt;&gt;Action&lt;br /&gt;Adventure&lt;br /&gt;Animation&lt;br /&gt;Comedy&lt;br /&gt;Crime&lt;br /&gt;Documentary&lt;br /&gt;Drama&lt;br /&gt;Family&lt;br /&gt;Fantasy&lt;br /&gt;History&lt;br /&gt;Horror&lt;/&gt;"
                    shape="RoundedBorder4"
                    variant="OutlineWhiteA7007f"
                  ></CheckBox>
                  <div className="absolute bg-red_500 border border-solid border-white_A700_7f flex flex-col h-3.5 items-center justify-start p-0.5 right-[0] rounded top-[1%] w-3.5">
                    <Img
                      src="images/img_checkmark.svg"
                      className="h-1.5 my-0.5"
                      alt="checkmark"
                    />
                  </div>
                  <div className="absolute bg-white_A700_33 border border-solid border-white_A700_7f h-3.5 right-[0] rounded top-[10%] w-3.5"></div>
                  <div className="absolute bg-white_A700_33 border border-solid border-white_A700_7f h-3.5 right-[0] rounded top-[20%] w-3.5"></div>
                  <div className="absolute bg-white_A700_33 border border-solid border-white_A700_7f h-3.5 right-[0] rounded top-[29%] w-3.5"></div>
                  <div className="absolute bg-white_A700_33 border border-solid border-white_A700_7f h-3.5 right-[0] rounded top-[39%] w-3.5"></div>
                  <div className="absolute bg-white_A700_33 border border-solid border-white_A700_7f bottom-[38%] h-3.5 right-[0] rounded w-3.5"></div>
                  <div className="absolute bg-white_A700_33 border border-solid border-white_A700_7f bottom-[29%] h-3.5 right-[0] rounded w-3.5"></div>
                  <div className="absolute bg-white_A700_33 border border-solid border-white_A700_7f bottom-[19%] h-3.5 right-[0] rounded w-3.5"></div>
                  <div className="absolute bg-white_A700_33 border border-solid border-white_A700_7f bottom-[10%] h-3.5 right-[0] rounded w-3.5"></div>
                  <div className="absolute bg-white_A700_33 border border-solid border-white_A700_7f bottom-[0] h-3.5 right-[0] rounded w-3.5"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-col items-end mt-[61px] md:px-10 sm:px-5 px-[496px] w-full">
          <Button
            className="cursor-pointer font-semibold leading-[normal] min-w-[151px] text-center text-gray_300 text-sm"
            shape="CircleBorder16"
            size="sm"
            variant="FillRedA700"
          >
            Load More
          </Button>
        </div>
        <div className="flex flex-col items-center mt-[93px] w-full">
          <Footer className="bg-black_900_49 flex items-center justify-center md:px-5 w-full" />
        </div>
      </div>
    </>
  );
};

export default PageList;
